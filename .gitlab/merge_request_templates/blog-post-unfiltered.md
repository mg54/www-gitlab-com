<!-- PLEASE READ: See https://about.gitlab.com/handbook/marketing/blog/unfiltered for details on the Unfiltered blog process -->

<!-- All blog posts should have a corresponding issue. Create an issue now if you haven't already, and add the link in place of the placeholder link below -->
Closes https://gitlab.com/gitlab-com/www-gitlab-com/issues/your-issue-number-here

<!-- Pro tip: If you begin your MR title with "Blog post:" your review app will be ready much faster. -->

### Checklist for writer

- [ ] Link to issue added, and set to close when this MR is merged
- [ ] Add disclaimer copy to the top of your blog post file: https://about.gitlab.com/handbook/marketing/blog/unfiltered/#link-to-disclaimer
- [ ] Blog post file formatted correctly, including any accompanying images
- [ ] `unfiltered` category entered in frontmatter
- [ ] Review app checked for any formatting issues
- [ ] Reviewed by fellow team member
- [ ] Filename updated to reflect current date (see https://about.gitlab.com/handbook/marketing/blog/#making-changes-to-your-blog-post)
- [ ] Assign to someone on your team with maintainer access to merge (no need to assign to an Editorial team member)

/label ~"unfiltered"
