---
layout: markdown_page
title: "Neurodiversity Resources"
description: "This page provides resources for team members, managers and those who identify as neurodiverse"
canonical_path: "/company/culture/inclusion/neurodiversity-resources/"
---

## The purpose of Neurodiversity Resources 

- Provide resources for those who identify as neurodiverse, to help them navigate remote work, management difficulties and other situations and challenges that they may face at work. 
- Provide team members and managers+ resources to help provide a better team member experience for there colleagues and direct reports who identify as neurodiverse. 

## Resources for those who identify at Neurodiverse
- [Neurodiverse is a competitive advantage](https://hbr.org/2017/05/neurodiversity-as-a-competitive-advantage)
- [ADHD in the workplace](https://www.webmd.com/add-adhd/adhd-in-the-workplace)

## Resources for Team Members and Managers+

- [How to embed neurodiversity into your people management practices](https://www.hrzone.com/perform/people/how-to-embed-neurodiversity-into-your-people-management-practices)
- [Austistic Advocacy Reources](https://autisticadvocacy.org/resources/accessibility/)
- [Certified Neurodiverse Workplace](https://ibcces.org/certified-neurodiverse-workplace/)
- [Understanding the benefits of neurodiversity in the workplace](https://www.hays.com.au/blog/insights/understanding-the-benefits-of-neurodiversity-in-the-workplace)
- [Neurodiversity Resources for Employers](https://www.neurodiversityhub.org/resources-for-employers)


## Reasonable Accommodations

We provide reasonable accommodations to individuals in order to remove any artificial barriers to success.  Our [EAP program](https://about.gitlab.com/handbook/total-rewards/benefits/modern-health/) is always available for team members to help them identify the best reasonable accommodations for them.

_Please note that all accommodations discussed below are potential accommodations that can be reviewed on a case-by-case basis, but are not guaranteed. Any accommodations and/or adjustments made are in line with the applicable laws/regulations of the jurisdiction in question._

### Evaluating Performance

It is important to have a discussion with the team member regarding the best way for them to receive feedback.  Although the performance standards would not be changed, the manager might need to change their approach. Reflecting on your communication style as a manager and tailoring this to best suit the individual's need can be a good place to start. There is no one size fits all approach when it comes to communication, but here are a few ideas:

* Conducting more frequent one-to-one meetings
* Delivering feedback visually structured in a way (tables, color coding) that is easier to digest 
* Make all types of feedback direct and clear

### Coaching

* Recorded instructions that can be reviewed later

### Time Management

 * Assign a mentor
 * Provide to-do lists
 * Assistance with prioritization
 * Assistive technology (timers, apps, calendars, etc)

 ### References for more ideas

 * [Neurodiversity in the Workplace](https://askearn.org/topics/neurodiversity-in-the-workplace/#1557151728256-a74a15bb-64c5)

* [Attention Deficit/ Hyperactivity Disorder (AD/HD)](https://askjan.org/disabilities/Attention-Deficit-Hyperactivity-Disorder-AD-HD.cfm)


